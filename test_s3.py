import pytest
import upload
import boto3
from moto import mock_s3

@mock_s3
def test_upload():
    conn = boto3.client('s3', region_name='us-east-1')
    # We need to create the bucket since this is all in Moto's 'virtual' AWS account
    conn.create_bucket(Bucket='comp630-m1-f19')
    upload.upload_file("ash1025.boto", "comp630-m1-f19", "ash1025.boto")

    assert True